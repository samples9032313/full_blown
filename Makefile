HOSTNAME ?= sample_full_blown_image
IMAGE_NAME ?= ${HOSTNAME}:1.0
CONTAINER_NAME ?= ${HOSTNAME}

# build project docker image
image:
	docker build -t ${IMAGE_NAME} \
		--build-arg UID=`id -u` \
		.

# start project docker container
start:
	# create image if it is not already there
	docker images|grep ${HOSTNAME} \
	|| make image
	
	docker run -it --name ${CONTAINER_NAME} --detach \
		--volume `realpath .`:/source --workdir /source \
		--network host \
		--hostname ${HOSTNAME} \
		${IMAGE_NAME} \
	|| \
	docker start ${CONTAINER_NAME}

# stop project docker container
stop:
	docker stop ${CONTAINER_NAME}

# join into project docker container with console
join: start
	docker exec -it ${CONTAINER_NAME} /bin/bash

# remove project docker container
rm: stop
	-docker rm ${CONTAINER_NAME}

# remove both project image and container
purge: rm
	-docker rmi ${IMAGE_NAME}

# clean-up project inside docker container
clean: start
	docker exec ${CONTAINER_NAME} rm -rf build

# build project inside docker container
build: start clean
	docker exec ${CONTAINER_NAME} cmake -S . -B build
	docker exec ${CONTAINER_NAME} cmake --build build -j16

# run project unit tests inside docker container
test: start
	docker exec ${CONTAINER_NAME} ctest --test-dir build --output-on-failure --output-junit junit.report

# generate coverage report in `build-coverage/coverage` diractory
coverage: start
	docker exec ${CONTAINER_NAME} cmake -S . -B build-coverage -DCMAKE_BUILD_TYPE=Coverage
	docker exec ${CONTAINER_NAME} cmake --build build-coverage -j16 -t coverage

# run static/dynamic analyze inside docker container
analyze:
	docker exec ${CONTAINER_NAME} cppcheck -j16 --enable=all --inconclusive --inline-suppr --xml --xml-version=2 --output-file=cppcheck.report .

doc:
	docker exec ${CONTAINER_NAME} cmake --build build -j16 -t doxygen

.PHONY: image start join stop rm purge clean build test coverage analyze
