FROM ubuntu:22.04

ARG UID
ARG USER=developer

RUN apt-get update -y ;\
	apt-get install -y \
		locales \
		pkg-config \
		gcc \
		g++ \
		cmake \
		git

# set locales
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# tools
RUN apt-get update -y ;\
	DEBIAN_FRONTEND=noninteractive apt-get install -y \
		gcovr \
		clang-tidy \
		cppcheck \
		doxygen \
		graphviz

# project dependencies
RUN apt-get update -y ;\
	DEBIAN_FRONTEND=noninteractive apt-get install -y \
		catch2

# create $USER user (and change password to $USER)
RUN useradd -d /home/${USER} -l -U -G sudo -m -s /bin/bash -u ${UID} ${USER}
RUN echo "root:${USER}" | chpasswd
RUN echo "${USER}:${USER}" | chpasswd

VOLUME [ "/source" ]

# switch to user
USER ${USER}
WORKDIR /home/${USER}
