# Adds address sanitizer to target
function(AddSanitizer target)
	# enable address sanitizer for Debug build
	if (CMAKE_BUILD_TYPE STREQUAL Debug)
		# TODO: enable also other sanitizers (like undefined) 
		target_compile_options(${target} PRIVATE -fsanitize=address)
		target_link_options(${target} PUBLIC -fsanitize=address)
	endif()
endfunction()