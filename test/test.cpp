#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "concurrent_queue.hpp"

TEST_CASE("we want to generate some warningst there", 
	"[warning]") {
	int unused_variable = 42;
	int uninitialized_variable;
	REQUIRE(true);
}

TEST_CASE("we can push and pop elements to the queue", 
	"[concurrent_queue]") {
	concurrent_queue<int> q;
	REQUIRE(q.empty());
	
	q.push(2);
	q.push(5);
	REQUIRE(!q.empty());

	int task;
	REQUIRE(q.try_pop(task));
	REQUIRE(q.try_pop(task));
	REQUIRE(q.empty());
}

// not run by default use [memory-leak] tag to trigger it
TEST_CASE("should trigger address sanitizer report", 
	"[.][memory-leak]") {

	{
		int * __leak = new int;  // memory leak
	}

	REQUIRE(true);
}