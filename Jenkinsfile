pipeline {
	agent any

	stages {
		stage("docker image") { steps {
			sh 'make image'
		}}

		stage("build") { steps {
			sh 'make build'
		}}
		
		stage("test & analyze") { steps {
			sh 'make test'
			sh 'make analyze'
			sh 'make coverage'
			sh 'make doc'
		}}
	}
	
	post { 
		always {
			// cleanup
			sh 'make rm'

			// reports
			junit 'build/junit.report'
			recordIssues tool: gcc()

			// TODO: replace $class
			emailext body: '${JELLY_SCRIPT, template="text"}', subject: '$DEFAULT_SUBJECT', recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']]
		}

		success {
			// generate test coverage report
			cobertura coberturaReportFile: 'build-coverage/coverage/cobertura.report'

			// generate cppcheck report
			recordIssues tool: cppCheck(pattern: 'cppcheck.report')
			
			// publish documentaiton (Main Page, Classes)
			publishHTML(target : [allowMissing: false,
				alwaysLinkToLastBuild: true,
				keepAll: true,
				reportDir: 'build/doc',
				reportFiles: 'index.html, annotated.html',
				reportTitles: 'Main Page, Classes',
				reportName: 'Documentation'])
		}
	}
}
